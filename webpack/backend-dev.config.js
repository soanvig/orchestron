const webpack = require('webpack');
const merge = require('webpack-merge');
const FormatMessagesWebpackPlugin = require('format-messages-webpack-plugin');
const backend = require('./backend.config');

module.exports = merge(backend, {
  mode: 'development',
  entry: ['webpack/hot/poll?100'],
  watch: true,
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new FormatMessagesWebpackPlugin()
  ],
  stats: 'minimal'
});
