const path = require('path');
const merge = require('webpack-merge');
const FormatMessagesWebpackPlugin = require('format-messages-webpack-plugin');
const frontend = require('./frontend.config');

module.exports = merge(frontend, {
  mode: 'development',
  watch: true,
  plugins: [
    new FormatMessagesWebpackPlugin()
  ],
  devServer: {
    contentBase: path.join(__dirname, '../dist'),
    compress: false,
    port: 9000,
    stats: 'minimal'
  }
});
