const merge = require('webpack-merge');
const frontend = require('./frontend.config');

module.exports = merge(frontend, {
  mode: 'production',
});
