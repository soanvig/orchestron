const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const base = require('./base.config');

module.exports = merge(base, {
  entry: ['./src/frontend/index.tsx'],
  output: {
    filename: 'frontend.js',
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
        template: './src/frontend/frontend.html'
    })
  ],
});
