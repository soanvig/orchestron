const merge = require('webpack-merge');
const backend = require('./backend.config');

module.exports = merge(backend, {
  mode: 'production',
  optimization: {
    minimize: false,
  },
});
