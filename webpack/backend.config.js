const merge = require('webpack-merge');
const nodeExternals = require('webpack-node-externals');
const base = require('./base.config');

module.exports = merge(base, {
  target: 'node',
  node: {
    __dirname: false,
    __filename: false,
  },
  entry: ['./src/backend/index.ts'],
  output: {
    filename: 'backend.js',
    libraryExport: 'default',
    libraryTarget: 'umd',
  },
  externals: [
    nodeExternals({
      whitelist: ['webpack/hot/poll?100'],
    }),
  ],
});
