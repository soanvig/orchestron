# Orchestron

<div align="center">
<img src="https://badge.fury.io/js/orchestron.svg" alt="Package version">

<div>
<a href="https://nodei.co/npm/orchestron/">
<img src="https://nodei.co/npm/orchestron.png?downloads=true&downloadRank=true" alt="">
</a>
</div>

<h2>Organize project commands into one managable interface</h2>
</div>

<!-- TOC -->

- [Orchestron](#orchestron)
  - [What is it about?](#what-is-it-about)
  - [Example processes configuration](#example-processes-configuration)
  - [Installation](#installation)
  - [Usage](#usage)
  - [Configuration](#configuration)
  - [Custom interface](#custom-interface)
  - [Known issues](#known-issues)

<!-- /TOC -->

## What is it about?

Orchestron allows to add commands (aka processes) to any project, and provides interface to manage them through websockets.

Using built-in interface is not the requirement, if you can provide your own.

## Example processes configuration

```js
// orchestron.config.json
{
  "processes": [
    {
      "name": "Regenerate",
      "command": "./scripts/regenerate.sh",
      "arguments": ["*"]
    },
    {
      "name": "Build",
      "command": "npm",
      "arguments": ["run build"]
    }
  ]
}
```

![Example processes interface](./misc/readme-processes.png)

## Installation

`npm install --only=prod --save-dev orchestron`

`yarn add --dev orchestron`

## Usage

Orchestron can be used as standalone application, or in conjuction with already created app.

1. In both cases you need to [create](#configuration) `orchestron.config.json` file in your **working directory**.
2. Then, you need to import Orchestron, and launch it:
  ```js
  const orchestron = require('orchestron'); // Orchestron may also be imported as any UMD-compatible module system, like ES6
  const port = 9999; // select port to start app on (default is 8123)
  const configLocation = '../my-config.json'; // select config lcation (default is orchestron.config.json)
  orchestron({ port, configLocation });
  ```
3. Navigate to `localhost:<port>` and fiddle with built-in interface.

## Configuration

Orchestron after launching will look for `orchestron.config.json` file in working directory, that means, directory from which script is executing (not the directory the script is contained in).

Orchestron performs configuration file validation, and if it's invalid, process won't start and user will be informed about error.

Example configuration:
```js
{
  "processes": [ // Array of processes objects
    {
      "name": "Build", // Name of the process, needs to be unique.
      "command": "npm", // Command to launch. It may be important to keep it as single-word/path.
      "arguments": ["run build"], // Array of arguments, that command will be provided with
      "autostart": false // Default: false. If set to true command will autolaunch on Orchestron launch
    }
  ]
}
```

## Custom interface

[Official documentation](./docs/interface.md)

One can also see how built-in interface communicates with backend via websockets in [./src/frontend/services/Api.ts](./src/frontend/services/Api.ts) file.

## Known issues

For list of all known issues see [GitLab Issues](https://gitlab.com/soanvig/orchestron/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=bug)
