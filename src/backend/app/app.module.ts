
import { Module, Global } from '@nestjs/common';
import { ProcessApiModule } from '~/backend/processApi/processApi.module';
import { appProviders } from '~/backend/app/app.providers';
import { FrontendServerModule } from '~/backend/frontendServer/frontendServer.module';

/**
 * Module that joins together all system modules.
 */
@Global()
@Module({
  imports: [
    FrontendServerModule,
    ProcessApiModule,
  ],
  providers: appProviders,
  exports: appProviders,
})
export class ApplicationModule {}
