import { Injectable } from '@nestjs/common';
import { Subject, Observable } from 'rxjs';

/**
 * Service providing useful utils across application.
 */
@Injectable()
export class UtilsService {

  /**
   * Creates Observable wrapper around Subject that pipes next/error messages from Subject to Observable.
   *
   * @param subject subject to be wrapped
   */
  public createSubjectObservable<T> (subject: Subject<T>): Observable<T> {
    return new Observable((ob) => {
      subject.subscribe({
        next (message: T) { ob.next(message); },
        error (message: T) { ob.error(message); },
        complete () { ob.complete(); },
      });
    });
  }
}
