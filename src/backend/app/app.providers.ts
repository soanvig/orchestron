import { UtilsService } from '~/backend/app/utils.service';
import { Provider } from '@nestjs/common';
import { UtilsServiceToken } from '~/tokens';

export const utilsServiceProvider: Provider = {
  provide: UtilsServiceToken,
  useClass: UtilsService,
};

export const appProviders: Provider[] = [
  utilsServiceProvider,
];
