
import { Test } from '@nestjs/testing';
import { configurationProviders } from '~/backend/configuration/configuration.providers';
import { ConfigurationService } from '~/backend/configuration/configuration.service';
import { ConfigurationServiceToken } from '~/tokens';

describe('ConfigurationService', () => {
  let configurationService: ConfigurationService;

  beforeEach(async () => {
    const module = await Test
      .createTestingModule({ providers: configurationProviders })
      .compile();

    configurationService = module.get<ConfigurationService>(ConfigurationServiceToken);
  });

  describe('loadConfiguration', () => {
    it('should throw if no configuration is provided', async () => {
      await expect(configurationService.loadConfiguration(null))
        .rejects
        .toThrow(Error);
    });

    it('should throw if invalid configuration is provided', async () => {
      await expect(configurationService.loadConfiguration({ test: 666 }))
        .rejects
        .toThrow(Error);

      await expect(configurationService.loadConfiguration({ notExistingField: 'string' }))
        .rejects
        .toThrow(Error);
    });

    it('should return true if correct configuration is provided', async () => {
      const result = await configurationService.loadConfiguration({ test: 'test' });
      expect(result)
        .toBe(true);
    });

  });
});
