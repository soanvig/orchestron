
import { IsString, IsArray, ValidateNested, IsOptional } from 'class-validator';
import { ConfigurationProcess } from '~/backend/configuration/configuration.dto';
import { Type } from 'class-transformer';

export class ConfigurationDtoMock {

  @IsString()
  test: string;

  @IsOptional()
  @IsArray()
  @ValidateNested()
  @Type(() => ConfigurationProcess)
  processes?: ConfigurationProcess[];
}
