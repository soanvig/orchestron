import { Type } from 'class-transformer';
import { ValidateNested, IsString, IsDefined, IsOptional, IsBoolean, IsArray } from 'class-validator';

/**
 * Configuration DTO, created from configuration JSON
 */
export class ConfigurationDto {
  @IsDefined()
  @IsArray()
  @ValidateNested()
  @Type(() => ConfigurationProcess)
  processes: ConfigurationProcess[];
}

/**
 * Configuration of single process
 */
export class ConfigurationProcess {
  @IsString()
  name: string;

  @IsString()
  command: string;

  @IsOptional()
  @IsBoolean()
  autostart?: boolean = false;

  @IsOptional()
  @IsArray()
  @IsString({
    each: true,
  })
  arguments?: string[] = [];
}
