
import { Injectable, Inject, OnModuleInit } from '@nestjs/common';
import { ConfigurationDto } from '~/backend/configuration/configuration.dto';
import * as ClassTransformer from 'class-transformer';
import * as ClassValidator from 'class-validator';
import { ConfigurationDtoToken } from '~/tokens';
import * as Fs from 'fs';

@Injectable()
export class ConfigurationService implements OnModuleInit {

  @Inject(ConfigurationDtoToken)
  private fConfigDto: typeof ConfigurationDto;

  /** Private instance of configuration */
  private fConfig: ConfigurationDto;

  /** Config location */
  private fConfigLocation: string = './orchestron.config.json';

  /**
   * Returns readonly version of config
   */
  public get config (): Readonly<ConfigurationDto> {
    return this.fConfig;
  }

  /**
   * Validates and loads configuration into memory.
   *
   * @param plainConfig configuration literal object
   */
  public async loadConfiguration (plainConfig: Record<string, any>): Promise<boolean> {
    if (!plainConfig) {
      throw new Error('No configuration provided');
    }

    const config: ConfigurationDto = ClassTransformer.plainToClass(this.fConfigDto, plainConfig);

    const validationErrors: ClassValidator.ValidationError[] = await ClassValidator.validate(config);

    if (validationErrors.length === 0) {
      this.fConfig = config;

      return true;
    } else {
      throw new Error(`Configuration loading error. Invalid configuration object. ${validationErrors}`);
    }
  }

  /**
   * Allows to register custom config location.
   * This function should be called **before** app initialization.
   *
   * @param location location of the config
   */
  public registerCustomConfig (location: string): void {
    this.fConfigLocation = location;
  }

  /**
   * Lifehook called during initialization.
   * This won't and shouldn't be called during unit tests.
   */
  public async onModuleInit (): Promise<void> {
    await this.initializeConfig();
  }

  /**
   * Initializes config from `config.json` file.
   */
  private initializeConfig (): Promise<void> {
    return new Promise((resolve) => Fs.readFile(this.fConfigLocation, (err, contents) => {
      if (err) {
        throw new Error(err.message);
      }

      const plainConfig: Record<string, any> = JSON.parse(contents.toString());

      this.loadConfiguration(plainConfig)
        .then(() => resolve())
        .catch((e) => { throw new Error(e.message); });
    }));
  }
}
