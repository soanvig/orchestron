import { Provider } from '@nestjs/common';
import { ConfigurationServiceToken, ConfigurationDtoToken } from '~/tokens';
import { ConfigurationService } from '~/backend/configuration/configuration.service';
import { ConfigurationDto } from '~/backend/configuration/configuration.dto';
import { ConfigurationDtoMock } from '~/backend/configuration/configuration.dto.mock';

export const configurationServiceProvider: Provider = {
  provide: ConfigurationServiceToken,
  useClass: ConfigurationService,
};

export const configurationDtoProvider: Provider = {
  provide: ConfigurationDtoToken,
  useValue: process.env.NODE_ENV === 'test' ? ConfigurationDtoMock : ConfigurationDto,
};

export const configurationProviders: Provider[] = [
  configurationServiceProvider,
  configurationDtoProvider,
];
