
import { Module } from '@nestjs/common';
import { configurationProviders, configurationServiceProvider } from '~/backend/configuration/configuration.providers';

/**
 * Configuration module responsible for loading, managing and sharing configuration.
 */
@Module({
  providers: configurationProviders,
  exports: [configurationServiceProvider],
})
export class ConfigurationModule {}
