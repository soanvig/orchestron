import { NestFactory } from '@nestjs/core';
import { ApplicationModule } from '~/backend/app/app.module';
import { ConfigurationServiceToken } from '~/tokens';
import { ConfigurationService } from '~/backend/configuration/configuration.service';

declare const module: any;

interface IOrchestronBackend {
  close: () => void;
}

interface IOrchestronParams {
  port?: number;
  configLocation?: string;
}

export default async function StartOrchestronBackend (
  { port = 8123, configLocation }: IOrchestronParams = {},
): Promise<IOrchestronBackend> {
  const app = await NestFactory.create(ApplicationModule);

  if (configLocation) {
    // Getting config service before app start allows to configure custom custom location
    const configService = app.get<ConfigurationService>(ConfigurationServiceToken);
    configService.registerCustomConfig('./asd.json');
  }

  await app.listen(port);

  if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => app.close());
  }

  return {
    close: () => app.close(),
  };
}

// Autostart orchestron for development
if (process.env.NODE_ENV === 'development') {
  StartOrchestronBackend()
    .catch(e => console.log('error', e));
}
