
import { Provider } from '@nestjs/common';
import { ProcessGatewayToken } from '~/tokens';
import { ProcessApiGateway } from '~/backend/processApi/processApi.gateway';

export const processApiProviders: Provider[] = [
  {
    provide: ProcessGatewayToken,
    useClass: ProcessApiGateway,
  },
];
