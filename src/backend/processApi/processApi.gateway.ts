
import { WebSocketGateway, OnGatewayConnection, OnGatewayInit, OnGatewayDisconnect, SubscribeMessage, WsException } from '@nestjs/websockets';
import { TProcessExecuteRequest, ProcessEvent, TProcessOutput, ProcessCommand } from '~/types/process';
import { Inject, OnModuleInit } from '@nestjs/common';
import { ProcessManagerServiceToken } from '~/tokens';
import { ProcessManagerService } from '~/backend/processManager/processManager.service';
import { Process } from '~/backend/processManager/process';

@WebSocketGateway({ namespace: '/process' })
export class ProcessApiGateway implements OnGatewayConnection, OnGatewayDisconnect, OnModuleInit {

  /** Private memory of clients */
  private fClients: SocketIO.Socket[] = [];

  @Inject(ProcessManagerServiceToken)
  private fProcessManager: ProcessManagerService;

  /**
   * Launches after gateway initialization
   */
  public onModuleInit (): void {
    this.fProcessManager.processOutput.subscribe((msg) => this.emitProcessOutput(msg, this.fClients));
  }

  /**
   * Handles connection. Saves client.
   *
   * @param client client session
   * @param data initial data sent by client
   */
  public handleConnection (client: SocketIO.Socket): void {
    console.log('Client connected');

    this.fClients.push(client);
  }

  /**
   * Handles disconnection. Removes clients from memory.
   *
   * @param client client session
   */
  public handleDisconnect (client: SocketIO.Socket): void {
    console.log('Client disconnected');

    const index: number = this.fClients.findIndex(c => c.id === client.id);
    this.fClients.splice(index, 1);
  }

  /**
   * Listens on execute event from client.
   * Proxies request to other services.
   *
   * @param client client session
   * @param data data of given command and it's payload
   * @returns `true` if request was handled, `false` if request was denied, throws for invalid request
   */
  @SubscribeMessage(ProcessCommand.EXECUTE)
  public onExecute (client: SocketIO.Socket, data: TProcessExecuteRequest): boolean {
    if (typeof data === 'undefined' || typeof data.command === 'undefined') {
      throw new WsException('Invalid execute message. No command provided.');
    }

    const clients: SocketIO.Socket[] = client ? [client] : this.fClients;

    try {
      // Proxy request to appropriate method
      switch (data.command) {
        case ProcessEvent.STATUS:
          this.emitCompleteStatus(clients);
          return true;
        case ProcessEvent.START:
          this.executeStart(data.payload.name, clients);
          return true;
        case ProcessEvent.STOP:
          this.executeStop(data.payload.name);
          return true;
        default:
          return false;
      }
    } catch (e) {
      throw new WsException(e.message);
    }
  }

  /**
   * Sends complete process status to given clients.
   *
   * @param clients clients to send status to, defaults to all clients
   */
  private emitCompleteStatus (clients: SocketIO.Socket[]): void {
    clients.forEach(client => client.emit(ProcessEvent.STATUS, this.fProcessManager.getCompleteStatus()));
  }

  /**
   * Sends output from process to given clients.
   *
   * @param clients clients to send status to, defaults to all clients
   * @param msg process output
   */
  private emitProcessOutput (msg: TProcessOutput, clients: SocketIO.Socket[]): void {
    clients.forEach(client => client.emit(ProcessEvent.OUTPUT, msg));
  }

  /**
   * Start command, starts given process.
   *
   * @param name name of the process to start
   */
  private executeStart (name: string, clients: SocketIO.Socket[]): void {
    const process: Process = this.fProcessManager.startProcess(name);

    // If process is going to be completed inform clients about status change
    // @TODO: API shouldn't listen on Process directly
    process.output.subscribe({
      complete: () => { this.emitCompleteStatus(clients); },
    });

    this.emitCompleteStatus(clients);
  }

  /**
   * Stop command, stops given process.
   *
   * @param name name of the process to stop
   */
  private executeStop (name: string): void {
    this.fProcessManager.killProcess(name);
  }
}
