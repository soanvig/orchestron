
import { Module } from '@nestjs/common';
import { processApiProviders } from '~/backend/processApi/processApi.providers';
import { ProcessManagerModule } from '~/backend/processManager/processManager.module';

/**
 * ProcessApi module is responsible for exposing process websocket gateway
 * and piping requests to ProcessManager
 */
@Module({
  providers: processApiProviders,
  imports: [ProcessManagerModule],
})
export class ProcessApiModule {}
