import { UtilsService } from '~/backend/app/utils.service';
import { Process } from '~/backend/processManager/process';
import { ProcessState, IProcessOutputMessage, IProcessOutputError } from '~/types/process';

describe('Process', () => {
  let utilsService: UtilsService;

  beforeEach(async () => {
    utilsService = new UtilsService();
  });

  test('should generate short id', () => {
    const process: Process = new Process(utilsService, 'echo', 'echo', 'orchestron');

    expect(process.id)
      .toBeTruthy();
  });

  test('should return proper fullCommand', () => {
    const process1: Process = new Process(utilsService, 'echo', 'echo', 'test');
    const process2: Process = new Process(utilsService, 'echo', 'echo');

    expect(process1.fullCommand)
      .toBe('echo test');

    expect(process2.fullCommand)
      .toBe('echo');
  });

  test('should return proper state', (done) => {
    const timeout: number = 1000;
    const process1: Process = new Process(utilsService, 'echo', 'echo');
    const process2: Process = new Process(utilsService, 'sleep', 'sleep', String(timeout / 1000 + 1));

    expect(process2.state)
      .toBe(ProcessState.ON);

    setTimeout(() => {
      expect(process1.state)
        .toBe(ProcessState.OFF);

      expect(process2.state)
        .toBe(ProcessState.ON);

      done();
    }, timeout);
  });

  test('should pipe correct stdout via echo', (done) => {
    const testingString: string = 'orchestron';
    const process: Process = new Process(utilsService, 'echo', 'echo', testingString);

    process.output.subscribe((result) => {
      expect((result as IProcessOutputMessage).message)
        .toBe(testingString);

      done();
    });
  });

  test('should pipe correct stdout via console.log', (done) => {
    const testingString: string = 'orchestron';
    const process: Process = new Process(
      utilsService,
      'node',
      'node',
      '-e',
      `'console.log("${testingString}")'`,
    );

    process.output.subscribe((result) => {
      expect((result as IProcessOutputMessage).message)
        .toBe(testingString);

      done();
    });
  });

  test('should pipe correct stderr', (done) => {
    const testingString: string = 'orchestron';
    const process: Process = new Process(
      utilsService,
      'node',
      'node',
      '-e',
      `'console.error("${testingString}")'`,
    );

    process.output.subscribe((error) => {
      expect((error as IProcessOutputError).error)
        .toBe(testingString);

      done();
    });
  });
});
