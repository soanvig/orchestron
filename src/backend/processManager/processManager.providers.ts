
import { Provider } from '@nestjs/common';
import { ProcessManagerServiceToken } from '~/tokens';
import { ProcessManagerService } from '~/backend/processManager/processManager.service';

export const processManagerServiceProvider: Provider = {
  provide: ProcessManagerServiceToken,
  useClass: ProcessManagerService,
};

export const processManagerProviders: Provider[] = [
  processManagerServiceProvider,
];
