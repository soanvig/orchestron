
import { spawn, ChildProcess } from 'child_process';
import { Subject, Observable } from 'rxjs';
import { chain as _chain } from 'lodash';
import { ProcessState, TProcessOutput } from '~/types/process';
import * as ShortId from 'shortid';
import { Inject } from '@nestjs/common';
import { UtilsServiceToken } from '~/tokens';
import { UtilsService } from '~/backend/app/utils.service';

/**
 * Wrapper around ChildProcess
 */
export class Process {

  /**
   * Creates an instance of Process.
   *
   * @param name name of the process. Common between multiple instances of that process
   * @param command command to be executed
   * @param params parameters for command
   */
  constructor (
    private gUtilsService: UtilsService,
    name: string,
    command: string,
    ...params: string[]
  ) {
    this.fId = ShortId.generate();
    this.fName = name;
    this.fCommand = command;
    this.fParams = params;

    this.fProcess = spawn(command, params, { shell: true });
    this.fIsRunning = true;

    this.fSubject = new Subject<TProcessOutput>();
    this.fOutput = this.gUtilsService.createSubjectObservable(this.fSubject);

    this.attachProcessHandlers();
  }

  /** Process id (unique to process instance) */
  private fId: string;

  /** Process name (can be common between many processes) */
  private fName: string;

  /** Command, that process was executed with */
  private fCommand: string;

  /** Params, that process was executed with */
  private fParams: string[];

  /** Flag determining if process is running */
  private fIsRunning: boolean = false;

  /** Stream observable, forwarding ouput from fSubject */
  private fOutput: Observable<TProcessOutput>;

  /** Subject called by process outputs and exit/close */
  private fSubject: Subject<TProcessOutput>;

  /** ChildProcess instance, created during construction */
  private fProcess: ChildProcess;

  public get output (): Observable<TProcessOutput> {
    return this.fOutput;
  }

  /**
   * Returns id
   */
  public get id (): string {
    return this.fId;
  }

  /**
   * Returns name
   */
  public get name (): string {
    return this.fName;
  }

  /**
   * Returns process state
   */
  public get state (): ProcessState {
    return this.fIsRunning ? ProcessState.ON : ProcessState.OFF;
  }

  /**
   * Returns full command that triggered processes (command + params)
   */
  public get fullCommand (): string {
    return [this.fCommand, ...this.fParams].join(' ');
  }

  /**
   * Kills process with given signal
   */
  public kill (signal: string = 'SIGTERM'): void {
    this.fProcess.kill(signal);
  }

  /**
   * Adds handler to process events.
   */
  private attachProcessHandlers (): void {

    if (!this.fProcess || !this.fSubject) {
      throw new Error('Process or subject is not defined. Cannot attach handlers.');
    }

    // Attach process quit handlers
    this.fProcess.on('exit', () => {
      this.fIsRunning = false;
      this.fSubject.complete();
    });
    this.fProcess.on('close', () => {
      this.fIsRunning = false;
      this.fSubject.complete();
    });
    this.fProcess.on('error', () => {
      this.fIsRunning = false;
      this.fSubject.complete();
    });

    // Attach process stderr and stdout handlers
    this.fProcess.stderr.on('data', (chunk: any) => this.fSubject.next(this.parseOutput(chunk, true)));
    this.fProcess.stdout.on('data', (chunk: any) => this.fSubject.next(this.parseOutput(chunk)));
  }

  /**
   * Takes given process output and pipes it through various parsers
   * Output is converted to string
   */
  private parseOutput (output: Buffer, error: boolean = false): TProcessOutput {
    const stringifiedOutput: string = String(output);

    const cleanOutput = _chain(stringifiedOutput)
      .trim()
      .value();

    if (error) {
      return {
        name: this.fName,
        error: cleanOutput,
      };
    }

    return {
      name: this.fName,
      message: cleanOutput,
    };
  }
}
