
import { Module } from '@nestjs/common';
import { processManagerProviders, processManagerServiceProvider } from '~/backend/processManager/processManager.providers';
import { ConfigurationModule } from '~/backend/configuration/configuration.module';

@Module({
  providers: processManagerProviders,
  exports: [
    processManagerServiceProvider,
  ],
  imports: [
    ConfigurationModule,
  ],
})
export class ProcessManagerModule {}
