
import { Injectable, OnModuleDestroy, OnModuleInit, Inject } from '@nestjs/common';
import { Process } from '~/backend/processManager/process';
import { IProcessStatus, TProcessOutput, ProcessState } from '~/types/process';
import { ConfigurationProcess } from '~/backend/configuration/configuration.dto';
import { ConfigurationServiceToken, UtilsServiceToken } from '~/tokens';
import { ConfigurationService } from '~/backend/configuration/configuration.service';
import { cloneDeep as _cloneDeep } from 'lodash';
import { Observable, Subject } from 'rxjs';
import { UtilsService } from '~/backend/app/utils.service';

@Injectable()
export class ProcessManagerService implements OnModuleDestroy, OnModuleInit {

  @Inject(ConfigurationServiceToken)
  private gConfigurationService: ConfigurationService;

  @Inject(UtilsServiceToken)
  private gUtilsService: UtilsService;

  /** Memory of created and running processes */
  private fProcessStack: Process[] = [];

  /** Available configured processes */
  private fConfiguredProcesses: ConfigurationProcess[] = [];

  /** Process output observable */
  private fProcessOutput: Observable<TProcessOutput>;

  /** Process output subject */
  private fProcessSubject: Subject<TProcessOutput>;

  /**
   * Returns observable for process outputs.
   */
  public get processOutput (): Observable<TProcessOutput> {
    return this.fProcessOutput;
  }

  /**
   * Starts one of the configured process.
   *
   * @param name name of the configured process to start
   */
  public startProcess (name: string): Process {
    const configuredProcess: ConfigurationProcess = this.fConfiguredProcesses.find(p => p.name === name);

    // If no process was found, don't proceed
    if (!configuredProcess) {
      return;
    }

    return this.createProcess(configuredProcess);
  }

  /**
   * Kills given process.
   *
   * @param processName name of process to kill
   */
  public killProcess (processName: string): boolean {
    const process: Process = this.fProcessStack.find(p => p.name === processName);

    // If not process was found, return false
    if (!process) {
      return false;
    }

    // Kill and remove from stack
    process.kill();

    return true;
  }

  /**
   * Retrevies status of all defined processes.
   */
  public getCompleteStatus (): IProcessStatus[] {
    return this.fConfiguredProcesses.map(process => this.getStatus(process.name));
  }

  /**
   * Retrieves all configured processes.
   */
  public getConfiguredProcesses (): ConfigurationProcess[] {
    return _cloneDeep(this.fConfiguredProcesses);
  }

  /**
   * Retrevies status of given process.
   */
  public getStatus (processName: string): IProcessStatus {
    const configuredProcess: ConfigurationProcess = this.findProcess(processName);

    if (!configuredProcess) {
      throw new Error(`Process of name "${processName}" doesn't exist`);
    }

    const process: Process = this.findProcessInStack(processName);

    return {
      name: configuredProcess.name,
      state: process ? ProcessState.ON : ProcessState.OFF,
    };
  }

  /**
   * Lifecycle hook, kills processes before destroying module.
   */
  public onModuleDestroy (): void {
    this.killAll();
  }

  /**
   * Lifecycle hook
   */
  public onModuleInit (): void {
    this.initialize();
  }

  /**
   * Loads configured processes into memory and creates subjects and observables.
   */
  public initialize (): void {
    this.fProcessSubject = new Subject<TProcessOutput>();
    this.fProcessOutput = this.gUtilsService.createSubjectObservable(this.fProcessSubject);

    this.fConfiguredProcesses = this.gConfigurationService.config.processes;

    // For each process that should autostart, start it
    this.fConfiguredProcesses
      .filter(p => p.autostart)
      .forEach(p => this.createProcess(p));
  }

  /**
   * Creates process.
   */
  private createProcess (configuration: ConfigurationProcess): Process {
    const process: Process = new Process(
      this.gUtilsService,
      configuration.name,
      configuration.command,
      ...configuration.arguments,
    );
    this.fProcessStack.push(process);

    // Subscribe to process output and pipe it to process subject
    // Kill process on complete
    process.output.subscribe({
      next: (output) => this.fProcessSubject.next(output),
      complete: () => this.removeProcessFromStack(process),
    });

    console.log(`Created process: ${configuration.name}`);

    return process;
  }

  /**
   * Kills all processes in stack.
   */
  private killAll (): void {
    this.fProcessStack.forEach(p => p.kill());
  }

  /**
   * Finds process in configured processes.
   *
   * @param processName name of the process
   */
  private findProcess (processName: string): ConfigurationProcess {
    return this.fConfiguredProcesses.find(p => p.name === processName);
  }

  /**
   * Looks for process in process stack.
   *
   * @param process process name or process instance
   */
  private findProcessInStack (process: string): Process {
    if (typeof process === 'string') {
      return this.fProcessStack.find(p => p.name === process);
    } else {
      return this.fProcessStack.find(p => p === process);
    }
  }

  /**
   * Removed given process from stack.
   *
   * @param process process to remove from stack.
   */
  private removeProcessFromStack (process: Process): void {
    this.fProcessStack = this.fProcessStack.filter(p => p !== process);
  }
}
