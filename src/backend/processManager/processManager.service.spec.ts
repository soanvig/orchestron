
import { Test } from '@nestjs/testing';
import { ProcessManagerServiceToken, ConfigurationServiceToken } from '~/tokens';
import { ApplicationModule } from '~/backend/app/app.module';
import { ConfigurationModule } from '~/backend/configuration/configuration.module';
import { ProcessManagerService } from '~/backend/processManager/processManager.service';
import { processManagerProviders } from '~/backend/processManager/processManager.providers';
import { ConfigurationService } from '~/backend/configuration/configuration.service';
import { ConfigurationProcess } from '~/backend/configuration/configuration.dto';
import { Process } from '~/backend/processManager/process';
import { ProcessState, IProcessStatus } from '~/types/process';
import { Observable } from 'rxjs';

const sleepProcessTime: number = 1000;

const sleepProcess: ConfigurationProcess = {
  name: 'test',
  command: 'sleep',
  arguments: [String(sleepProcessTime / 1000)],
  autostart: false,
};

const sleepProcessAutostart: ConfigurationProcess = {
  name: 'test-autostart',
  command: 'sleep',
  arguments: [String(sleepProcessTime / 1000)],
  autostart: true,
};

const processes: ConfigurationProcess[] = [sleepProcess, sleepProcessAutostart];

describe('ProcessManagerService', () => {
  let processManagerService: ProcessManagerService;

  beforeEach(async () => {
    const module = await Test
      .createTestingModule({
        providers: processManagerProviders,
        imports: [ConfigurationModule, ApplicationModule],
      })
      .compile();

    // Load configuration
    const configurationService: ConfigurationService = module.get<ConfigurationService>(ConfigurationServiceToken);
    await configurationService.loadConfiguration({
      processes,
      test: 'test',
    });

    processManagerService = module.get<ProcessManagerService>(ProcessManagerServiceToken);
    processManagerService.initialize();
  });

  describe('startProcess', () => {
    it('should return void if required process does not exist', () => {
      expect(processManagerService.startProcess('notReallyExistingName'))
        .toBeUndefined();
    });

    it('should return new Process if required process exists', () => {
      expect(processManagerService.startProcess(processes[0].name))
        .toBeInstanceOf(Process);
    });
  });

  describe('killProcess', () => {
    it('should return false if process is not running', () => {
      expect(processManagerService.killProcess(sleepProcess.name))
        .toBe(false);
    });

    it('should return true if process was running', () => {
      processManagerService.startProcess(sleepProcess.name);
      expect(processManagerService.killProcess(sleepProcess.name))
        .toBe(true);
    });

    it('should kill the process', (done) => {
      const process: Process = processManagerService.startProcess(sleepProcess.name);
      processManagerService.killProcess(sleepProcess.name);

      // Give time for process to close.
      setTimeout(() => {
        expect(process.state)
          .toBe(ProcessState.OFF);

        done();
      }, sleepProcessTime / 10);
    });
  });

  describe('getStatus', () => {
    it('should return status for given status', () => {
      const processStatus: IProcessStatus = processManagerService.getStatus(sleepProcessAutostart.name);

      expect(processStatus.name)
        .toBe(sleepProcessAutostart.name);

      const autostartProcessStatus: IProcessStatus = processManagerService.getStatus(sleepProcess.name);

      expect(autostartProcessStatus.name)
        .toBe(sleepProcess.name);
    });
  });

  describe('getCompleteStatus', () => {
    it('should return all statuses', () => {
      const status: IProcessStatus[] = processManagerService.getCompleteStatus();
      const statusName: string[] = status.map(s => s.name);

      expect(statusName)
        .toEqual(expect.arrayContaining([
          sleepProcess.name, sleepProcessAutostart.name,
        ]));
    });
  });

  describe('initialize', () => {
    it('should start processes with autostart', () => {
      const processStatus: IProcessStatus = processManagerService.getStatus(sleepProcessAutostart.name);

      expect(processStatus.state)
        .toBe(ProcessState.ON);
    });
  });

  describe('getConfiguredProcesses', () => {
    it('should return configured processes array', () => {
      const configuredProcesses: ConfigurationProcess[] = processManagerService.getConfiguredProcesses();

      expect(configuredProcesses)
        .toEqual(expect.arrayContaining(processes));
    });
  });
});
