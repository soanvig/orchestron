import { Controller, Get, Res } from '@nestjs/common';

@Controller()
export class FrontendServerController {
  @Get()
  serveFrontend (@Res() res) {
    res.sendFile(__dirname + '/index.html');
  }

  @Get('/frontend.js')
  serveLib (@Res() res) {
    res.sendFile(__dirname + '/frontend.js');
  }
}
