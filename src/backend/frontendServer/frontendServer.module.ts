import { Module } from '@nestjs/common';
import { FrontendServerController } from '~/backend/frontendServer/frontendServer.controller';

/**
 * FrontendServer module is responsible for serving frontend in production mode
 */
@Module({
  controllers: [FrontendServerController],
})
export class FrontendServerModule {}
