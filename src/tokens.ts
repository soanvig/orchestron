
export const ProcessGatewayToken = Symbol('ProcessGateway');

export const ProcessManagerServiceToken = Symbol('ProcessManagerService');

export const ConfigurationServiceToken = Symbol('ConfigurationService');
export const ConfigurationDtoToken = Symbol('ConfigurationDto');

export const UtilsServiceToken = Symbol('UtilsService');
