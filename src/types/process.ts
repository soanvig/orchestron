
export enum ProcessCommand {
  EXECUTE = 'EXECUTE',
}

export enum ProcessEvent {
  STATUS = 'STATUS',
  START = 'START',
  STOP = 'STOP',
  OUTPUT = 'OUTPUT',
}

export interface IProcessStatus {
  name: string;
  state: ProcessState;
}

export interface IProcessExecuteStatusRequest {
  command: ProcessEvent.STATUS;
}

export interface IProcessExecuteStartRequest {
  command: ProcessEvent.START;
  payload: {
    name: string;
  };
}

export interface IProcessExecuteStopRequest {
  command: ProcessEvent.STOP;
  payload: {
    name: string;
  };
}

export type TProcessExecuteRequest =
  IProcessExecuteStatusRequest
  | IProcessExecuteStartRequest
  | IProcessExecuteStopRequest;

interface IProcessOutputBase {
  name: string;
}

export interface IProcessOutputMessage extends IProcessOutputBase {
  message: string;
}

export interface IProcessOutputError extends IProcessOutputBase {
  error: string;
}

export type TProcessOutput = IProcessOutputMessage | IProcessOutputError;

export enum ProcessState {
  OFF,
  ON,
}
