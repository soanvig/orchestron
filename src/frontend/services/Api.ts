import * as Socket from 'socket.io-client';
import { ProcessEvent, IProcessStatus, TProcessOutput, ProcessCommand } from '~/types/process';

// In development mode target URL, in production mode target server hosting frontend
const apiUrl: string = (process.env.NODE_ENV === 'development') ? 'http://localhost:8123' : '';

/**
 * Connect to socket API
 */
export function connect (): SocketIOClient.Socket {
  return Socket(`${apiUrl}/process`);
}

/**
 * Listens on STATUS and calls callback if such received
 */
export function listenStatus (socket: SocketIOClient.Socket, cb: (status: IProcessStatus[]) => void): void {
  socket.on(ProcessEvent.STATUS, cb);
}

/**
 * Listens on OUTPUT and calls callback if such received
 */
export function listenOutput (socket: SocketIOClient.Socket, cb: (output: TProcessOutput) => void): void {
  socket.on(ProcessEvent.OUTPUT, cb);
}

/**
 * Sends STATUS command
 */
export function getStatus (socket: SocketIOClient.Socket): void {
  socket.emit(ProcessCommand.EXECUTE, {
    command: ProcessEvent.STATUS,
  });
}

/**
 * Sends START command
 * @param processName process name to start
 */
export function startProcess (socket: SocketIOClient.Socket, processName: string): void {
  socket.emit(ProcessCommand.EXECUTE, {
    command: ProcessEvent.START,
    payload: {
      name: processName,
    },
  });
}

/**
 * Sends STOP command
 * @param processName process name to stop
 */
export function stopProcess (socket: SocketIOClient.Socket, processName: string): void {
  socket.emit(ProcessCommand.EXECUTE, {
    command: ProcessEvent.STOP,
    payload: {
      name: processName,
    },
  });
}
