import * as React from 'react';
import { TProcessOutput, IProcessOutputError } from '~/types/process';
import './TerminalEntry.css';

interface ITerminalEntryProps {
  message: TProcessOutput;
}

/**
 * Returns whether given message is an error
 * @param message message to check for being an error
 */
function isError (message: TProcessOutput): message is IProcessOutputError {
  if ((message as IProcessOutputError).error) {
    return true;
  } else {
    return false;
  }
}

/**
 * Returns terminal entry css class
 *
 * @param isEntryError determines if entry is an error
 */
function getTerminalEntryClass (isEntryError: boolean): string {
  const baseClass: string[] = ['terminal-entry'];

  if (isEntryError) {
    baseClass.push('terminal-entry--error');
  }

  return baseClass.join(' ');
}

/**
 * TerminalEntry displays single line output in Terminal
 */
export const TerminalEntry: React.FunctionComponent<ITerminalEntryProps> = (props) => (
  <pre className={ getTerminalEntryClass(isError(props.message)) }>
    { isError(props.message) ? props.message.error : props.message.message }
  </pre>
);
