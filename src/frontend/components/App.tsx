import * as React from 'react';
import { Terminal } from '~/frontend/components/Terminal/Terminal';
import { IProcessStatus, TProcessOutput, IProcessOutputMessage } from '~/types/process';
import * as Api from '~/frontend/services/Api';

import '~/frontend/assets/styles/app.css';
import '~/frontend/assets/styles/button.css';
import '~/frontend/assets/styles/reset.css';

interface IState {
  processes: IProcessStatus[];
  socket: SocketIOClient.Socket;
  messages: TProcessOutput[];
}

export class App extends React.Component<{}, IState> {

  public state: IState;

  constructor (props) {
    super(props);

    this.state = {
      socket: null,
      processes: [],
      messages: [],
    };
  }

  public componentDidMount (): void {
    this.initialize();
  }

  public render (): JSX.Element {
    return (
      <div className='app'>
        { this.renderTerminals(this.state.processes) }
      </div>
    );
  }

  /**
   * Renders terminals based on given processes
   * @processes process list
   */
  private renderTerminals (processes: IProcessStatus[]): JSX.Element[] {
    return processes.map(p => (
      <Terminal
        state={ p.state }
        messages={ this.getProcessMessages(p.name) }
        title={ p.name }
        onStart={ () => this.onProcessStart(p.name) }
        onStop={ () => this.onProcessStop(p.name) }
        key={ p.name } />
    ));
  }

  /**
   * Initializes application:
   * - connects to socket
   * - enables listeners
   * - gets initial information
   */
  private initialize (): void {
    const socket: SocketIOClient.Socket = Api.connect();
    this.setState({ socket });

    Api.listenStatus(socket, (status) => this.catchStatus(status));
    Api.listenOutput(socket, (output) => this.catchOutput(output));

    Api.getStatus(socket);
  }

  /**
   * Handles status change
   * @param status new status
   */
  private catchStatus (status: IProcessStatus[]): void {
    this.setState({ processes: status });
  }

  /**
   * Handles process output
   * @param output process output
   */
  private catchOutput (output: TProcessOutput): void {
    this.setState({
      messages: this.state.messages.concat(output),
    });
  }

  /**
   * Handles processStart event
   * @param name name of the process that wants to start
   */
  private onProcessStart (name: string): void {
    Api.startProcess(this.state.socket, name);

    // Generate log message
    const newMessage: IProcessOutputMessage = {
      name,
      message: `LOG ::: Process ${name} started manually`,
    };

    this.setState({
      messages: this.state.messages.concat(newMessage),
    });
  }

  /**
   * Handles processStop event
   * @param name name of the process that wants to stop
   */
  private onProcessStop (name: string): void {
    Api.stopProcess(this.state.socket, name);

    // Generate log message
    const newMessage: IProcessOutputMessage = {
      name,
      message: `LOG ::: Process ${name} stopped manually`,
    };

    this.setState({
      messages: this.state.messages.concat(newMessage),
    });
  }

  /**
   * Returns messages for given process
   * @param name name of the process
   */
  private getProcessMessages (name: string): TProcessOutput[] {
    return this.state.messages.filter(m => m.name === name);
  }
}
