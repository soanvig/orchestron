import * as React from 'react';
import { ProcessState, TProcessOutput } from '~/types/process';
import { TerminalEntry } from '~/frontend/components/TerminalEntry/TerminalEntry';
import './Terminal.css';

interface ITerminalProps {
  title: string;
  messages: TProcessOutput[];
  state: ProcessState;
  onStart?: () => void;
  onStop?: () => void;
}

interface ITerminalEntryListProps {
  messages: TProcessOutput[];
  className?: string;
}

/**
 * TerminalEntry used as no output indicator
 */
const noEntriesMessage: JSX.Element = (
  <TerminalEntry message={ { name: '', message: 'No output' } } />
);

/**
 * TerminalEntryList is helper function rendering list of terminal entries.
 */
const TerminalEntryList: React.FunctionComponent<ITerminalEntryListProps> = (props) => {
  const listItems = props.messages.map((message, index) => (
    <TerminalEntry message={ message } key={ index } />
  ));

  return (
    <div className={ props.className }>
      { listItems.length ? listItems : noEntriesMessage }
    </div>
  );
};

/**
 * Returns terminal className based on process state (stopped/started)
 * @param state state of the process
 */
function terminalClassname (state: ProcessState): string {
  let className: string = 'terminal';

  if (state === ProcessState.OFF) {
    className += ' terminal--disabled';
  }

  return className;
}

/**
 * Terminal is responsible for displaying single terminal,
 * and multiple TerminalEntries.
 */
export const Terminal: React.FunctionComponent<ITerminalProps> = (props) => (
  <section className={ terminalClassname(props.state) }>
    <div className='terminal_header'>
      { props.title }

      <ul className='terminal_controls'>
        {
          (props.state === ProcessState.OFF) ? (
            <li>
              <button className='terminal_button' onClick={ props.onStart }>Start</button>
            </li>
          ) : (
            <li>
              <button className='terminal_button' onClick={ props.onStop }>Stop</button>
            </li>
          )
        }
      </ul>
    </div>

    <TerminalEntryList messages={ props.messages } className='terminal_content' />
  </section>
);
