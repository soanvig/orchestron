import * as ReactDOM from 'react-dom';
import * as React from 'react';
import { hot } from 'react-hot-loader/root';
import { App } from '~/frontend/components/App';

const AppHot = hot(App);

ReactDOM.render(
  <AppHot />,
  document.getElementById('app'),
);
