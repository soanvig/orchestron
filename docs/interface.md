# Interface

Interface serves as a tool for executing commands on backend server.

Communication is based on Websockets.

Below you can find interface for communicating with backend API, if you would like to build your interface.

<!-- TOC -->

- [Interface](#interface)
  - [Connection](#connection)
  - [Listen](#listen)
    - [Status](#status)
    - [Output](#output)
  - [Commands](#commands)
    - [Status](#status-1)
    - [Start process](#start-process)
    - [Start process](#start-process-1)

<!-- /TOC -->

## Connection

To setup connection with backend you need to connect to `<url>/process`, where `<url>` is the backend url (usually localhost).

## Listen

You need to listen on two messages

### Status

Status is the way for retrieving information about configured commands and their state.

After requesting status (or if backend decides to send it) of processes, event of name `STATUS` will be send to interface, containing message of given interface:

```ts
interface IProcessStatus {
  name: string;
  state: 0 | 1; // 0 means process is not running, 1 - is running
}

IProcessStatus[]
```

### Output

Backend will send process output to all connected clients. You can expect one of the two possible messages on `OUTPUT` event:

```ts
interface IProcessOutputMessage {
  name: string; // process name
  message: string;
}
```

```ts
interface IProcessOutputError {
  name: string; // process name
  error: string;
}
```

## Commands

To send any command you need to send event `EXECUTE` with following data:

```ts
{
  command: '<command name>',
  payload: {}, // optional, depends on given message
}
```

### Status

To retrieve process status manually execute:

```ts
{
  command: 'STATUS',
}
```

See [Listen/Status](#status) for more information.

### Start process

To start given process send:

```ts
{
  command: 'START',
  payload: {
    name: '<process name>',
  },
}
```

### Start process

To stop given process send:

```ts
{
  command: 'STOP',
  payload: {
    name: '<process name>',
  },
}
```
